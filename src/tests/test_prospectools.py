# tests/test_prospectools.py

import os
import unittest

from src.prospectool.prospectool import app


class ProspectoolsTests(unittest.TestCase):

    ############################
    #### setup and teardown ####
    ############################

    # executed prior to each test
    def setUp(self):
        self.app = app.test_client()

    # executed after each test
    def tearDown(self):
        pass


    ###############
    #### tests ####
    ###############

    def test_profiles(self):
        response = self.app.get('/profiles', follow_redirects=True)
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
