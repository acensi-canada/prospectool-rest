import json
from flask import Flask, jsonify, request;

app = Flask(__name__)

profiles_data = [
    {'firstname': 'John', 'lastname': 'Duff'},
    {'firstname': 'Alain', 'lastname': 'Térieur'},
    {'firstname': 'Ali', 'lastname': 'Gator'},
    {'firstname': 'Gaspard', 'lastname': 'Alizan'}
]

ALLOW_ORIGIN = (
    'https://prospectool-dev.herokuapp.com',
    'https://prospectool-prod.herokuapp.com',
    'http://localhost')

@app.route('/profiles')
def profiles():
    resp = jsonify(profiles_data)

    if 'Origin' in request.headers.keys() and request.headers['Origin'].lower().startswith((ALLOW_ORIGIN)) :
        resp.headers['Access-Control-Allow-Origin'] = request.headers['Origin']
        resp.headers['Access-Control-Allow-Methods'] ='GET, POST, DELETE, PUT, PATCH, OPTIONS'
        resp.headers['Access-Control-Allow-Headers']= 'Content-Type, api_key, Authorization'
        resp.headers['Access-Control-Allow-Credentials'] = 'true'
    return resp
