# ProspecTool-REST

## Prérequis
La version de python utilisée est [python-3.8.0](https://www.python.org/downloads/)

## Environnement virtuel Python
La création de l'environnement virtuel python n'est pas obligatoire mais conseillée.

L'environnement virtuel va permettre d'isoler les dépendances python spécifiques au projet évitant ainsi
les problèmes de version entre les packages installés sur votre machine.

### Création de l'environnement  virtuel
Pour créer l'environnement, placez vous à la racine des sources du repository et lancez la commande suivante :
```
python -m venv .
```

### Lancement de l'environnement virtuel
Pour lancer l'environnement lancez une des commandes suivantes :

- Sous Unix : `source bin/activate`
- Sous Windows
 - Git Bash : `source Scripts/activate`
 - CMD : `Scripts/activate.bat`
 - PowerShell : `Scripts/activate.ps1`

Plus d'information sur venv : https://docs.python.org/3/library/venv.html

### Installation des dépendances
Une fois l'environnement virtuel lancé, il faut installer les dépendances. Pour ce faire, lancer la commande suivante à la racine du repository :
```
pip install -r requirements.txt
```

## Lancement de l'application
Vous pouvez lancer l'application de deux manière différentes.

### Serveur Flask
Flask met à dispostion un serveur intégré au framework pour lancer les applications en local.

Lancez la commande suivante à la racine du repository :
```
python app.py
```

Vous pourrez accéder à l'API à l'URL suivante : http://localhost:5000/

### Serveur Gunicorn
Serveur web utilisé pour le déploiement sur les environnements de développement et de production.

**ATTENTION : Gunicorn ne fonctionne que sur les environnements Unix**

Pour lancer l'application avec Gunicorn, lancez la commande suivante à la racinne du repository :
```
gunicorn app:app --log-file=-
```
Vous pourrez accéder à l'API à l'URL suivante : http://localhost:8080/

## Environnements

|  Environnement | URL |
|---|---|
| Développement | https://prospectool-rest-dev.herokuapp.com/  |
| Production |  https://prospectool-rest-prod.herokuapp.com/  |
